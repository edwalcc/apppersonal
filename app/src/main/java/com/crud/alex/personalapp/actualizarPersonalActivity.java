package com.crud.alex.personalapp;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class actualizarPersonalActivity extends AppCompatActivity {
    protected Cursor cursor;
    DataHelper dbHelper;
    Button ton1, ton2;
    EditText text1, text2, text3, text4;

    Spinner spinnerCargo;
    ArrayList<String> listaCargos;
    String cargoSeleccionado;
    Spinner spinnerPais;
    ArrayList <String> listaPais;
    String paisSeleccionado;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actualizar_personal);

        dbHelper = new DataHelper(this);
        text1 = (EditText) findViewById(R.id.editText1);
        text2 = (EditText) findViewById(R.id.editText2);
        text3 = (EditText) findViewById(R.id.editText3);
        text4 = (EditText) findViewById(R.id.editText4);


        spinnerCargo= (Spinner) findViewById(R.id.spinner1);
        spinnerPais= (Spinner) findViewById(R.id.spinner2);

        listaCargos=getAllCargos();
        listaPais=getAllPais();

        ArrayAdapter<CharSequence> adapterCargo = new ArrayAdapter(this, android.R.layout.simple_spinner_item,listaCargos);
        ArrayAdapter <CharSequence> adapterPais = new ArrayAdapter(this, android.R.layout.simple_spinner_item,listaPais);

        spinnerCargo.setAdapter(adapterCargo);
        spinnerPais.setAdapter(adapterPais);

        spinnerCargo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cargoSeleccionado=listaCargos.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerPais.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                paisSeleccionado=listaPais.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        SQLiteDatabase db = dbHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM personal WHERE nombre = '" +
                getIntent().getStringExtra("nombre") + "'",null);
        cursor.moveToFirst();
        if (cursor.getCount()>0)
        {
            cursor.moveToPosition(0);
            text1.setText(cursor.getString(0).toString());
            text2.setText(cursor.getString(1).toString());
            text3.setText(cursor.getString(2).toString());
            text4.setText(cursor.getString(3).toString());
            paisSeleccionado=(cursor.getString(4).toString());
            cargoSeleccionado=(cursor.getString(5).toString());
        }
        ton1 = (Button) findViewById(R.id.button1);
        ton2 = (Button) findViewById(R.id.button2);
        // daftarkan even onClick pada btnSimpan
        ton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.execSQL("UPDATE personal set nombre='"+
                        text2.getText().toString() +"', dni='" +
                        text3.getText().toString()+"', fecNac='"+
                        text4.getText().toString() +"', pais='" +
                        paisSeleccionado+"', cargo='" +
                        cargoSeleccionado + "' where codPersonal='" +
                        text1.getText().toString()+"'");
                Toast.makeText(getApplicationContext(), "Se guardo exitosamente", Toast.LENGTH_LONG).show();
                PersonalActivity.pa.RefreshList();
                finish();
            }
        });
        ton2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                finish();
            }
        });
    }

    public ArrayList <String> getAllCargos(){
        ArrayList<String> cargos = new ArrayList <String>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM cargo", null);
        cursor.moveToFirst();
        for (int cc = 0; cc < cursor.getCount(); cc++) {
            cursor.moveToPosition(cc);
            cargos.add(cursor.getString(1).toString());
        }
        return cargos;
    }

    public ArrayList <String> getAllPais(){
        ArrayList<String> pais = new ArrayList<String>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM pais", null);
        cursor.moveToFirst();
        for (int cc = 0; cc < cursor.getCount(); cc++) {
            cursor.moveToPosition(cc);
            pais.add(cursor.getString(1).toString());
        }
        return pais;
    }
}
