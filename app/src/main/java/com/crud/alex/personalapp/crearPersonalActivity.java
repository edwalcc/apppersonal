package com.crud.alex.personalapp;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class crearPersonalActivity extends AppCompatActivity {
    protected Cursor cursor;
    DataHelper dbHelper;
    Button ton1, ton2;
    EditText text1, text2, text3, text4;
    Spinner spinnerCargo;
    ArrayList <String> listaCargos;
    String cargoSeleccionado;
    Spinner spinnerPais;
    ArrayList <String> listaPais;
    String paisSeleccionado;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_personal);

        dbHelper = new DataHelper(this);
        text1 = (EditText) findViewById(R.id.editText1);
        text2 = (EditText) findViewById(R.id.editText2);
        text3 = (EditText) findViewById(R.id.editText3);
        text4 = (EditText) findViewById(R.id.editText4);

        ton1 = (Button) findViewById(R.id.button1);
        ton2 = (Button) findViewById(R.id.button2);
        spinnerCargo= (Spinner) findViewById(R.id.spinner1);
        spinnerPais= (Spinner) findViewById(R.id.spinner2);

        listaCargos=getAllCargos();
        listaPais=getAllPais();

        ArrayAdapter <CharSequence> adapterCargo = new ArrayAdapter(this, android.R.layout.simple_spinner_item,listaCargos);
        ArrayAdapter <CharSequence> adapterPais = new ArrayAdapter(this, android.R.layout.simple_spinner_item,listaPais);

        spinnerCargo.setAdapter(adapterCargo);
        spinnerPais.setAdapter(adapterPais);

        spinnerCargo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cargoSeleccionado=listaCargos.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerPais.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                paisSeleccionado=listaPais.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




        ton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.execSQL("INSERT INTO personal(codPersonal, nombre, dni, fecNac, pais, cargo) values('" +
                        text1.getText().toString() + "','" +
                        text2.getText().toString() + "','" +
                        text3.getText().toString() + "','" +
                        text4.getText().toString() + "','" +
                        paisSeleccionado + "','" +
                        cargoSeleccionado + "')");
                Toast.makeText(getApplicationContext(), "Se guardo exitosamente", Toast.LENGTH_LONG).show();
                PersonalActivity.pa.RefreshList();
                finish();
            }
        });
        ton2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                finish();
            }
        });




    }

    public ArrayList <String> getAllCargos(){
        ArrayList<String> cargos = new ArrayList <String>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM cargo", null);
        cursor.moveToFirst();
        for (int cc = 0; cc < cursor.getCount(); cc++) {
            cursor.moveToPosition(cc);
            cargos.add(cursor.getString(1).toString());
        }
        return cargos;
    }

    public ArrayList <String> getAllPais(){
        ArrayList<String> pais = new ArrayList<String>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM pais", null);
        cursor.moveToFirst();
        for (int cc = 0; cc < cursor.getCount(); cc++) {
            cursor.moveToPosition(cc);
            pais.add(cursor.getString(1).toString());
        }
        return pais;
    }
}
