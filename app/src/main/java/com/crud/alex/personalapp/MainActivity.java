package com.crud.alex.personalapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridLayout;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    GridLayout mainGrid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainGrid = (GridLayout) findViewById(R.id.mainGrid);
        //Set Event
        setSingleEvent(mainGrid);

    }

    private void setSingleEvent(GridLayout mainGrid) {
        //Loop all child item of Main Grid
        for (int i=0; i<mainGrid.getChildCount();i++)
        {
            //Hijos de tipo CardView
            CardView cardView = (CardView) mainGrid.getChildAt(i);
            final int finalI=i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Remplazar
                    //Toast.makeText(MainActivity.this, "Clicked at index "+finalI, Toast.LENGTH_SHORT).show();
                    if(finalI==0)//Interfaz Cargo
                    {
                        //Toast.makeText(MainActivity.this, "Click en "+finalI, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(MainActivity.this, CargoActivity.class);
                        startActivity(intent);
                    }
                    if(finalI==1)//Interfaz Pais
                    {
                        //Toast.makeText(MainActivity.this, "Click en"+finalI, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(MainActivity.this, PaisActivity.class);
                        startActivity(intent);
                    }
                    if(finalI==2)//Interfaz Personal
                    {
                        Intent intent = new Intent(MainActivity.this, PersonalActivity.class);
                        startActivity(intent);
                    }
                }
            });
        }
    }


}
