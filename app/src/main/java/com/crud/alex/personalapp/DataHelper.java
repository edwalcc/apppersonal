package com.crud.alex.personalapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Creado por Edwin Alex on 6/12/2017
 */

public class DataHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "personalAPP.db";
    private static final int DATABASE_VERSION = 1;
    public DataHelper(Context context){
        super(context, DATABASE_NAME,null,DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db){
        String sqlPersonal = "CREATE TABLE personal(codPersonal INTEGER PRIMARY KEY, nombre TEXT NOT NULL, dni INTEGER, fecNac TEXT, pais INTEGER, cargo INTEGER);";
        String sqlPais = "CREATE TABLE pais(codPais INTEGER PRIMARY KEY, nombre TEXT NOT NULL);";
        String sqlCargo = "CREATE TABLE cargo(codCargo INTEGER PRIMARY KEY, nombre TEXT NOT NULL);";
        Log.d("Data","onCreate: "+sqlPersonal);
        Log.d("Data","onCreate: "+sqlPais);
        Log.d("Data","onCreate: "+sqlCargo);
        db.execSQL(sqlCargo);
        db.execSQL(sqlPais);
        db.execSQL(sqlPersonal);

    }
    
    @Override
    public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2){

    }

}
